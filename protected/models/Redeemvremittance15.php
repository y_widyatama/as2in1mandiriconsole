<?php

/**
 * This is the model class for table "redeemvremittance15".
 *
 * The followings are the available columns in table 'redeemvremittance15':
 * @property integer $id
 * @property string $id_member
 * @property string $nama_transaksi
 * @property string $voucher_no
 * @property string $tgl
 *
 * The followings are the available model relations:
 * @property Pelanggan $idMember
 */
class Redeemvremittance15 extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Redeemvremittance15 the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'redeemvremittance15';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_member', 'length', 'max'=>9),
			array('nama_transaksi', 'length', 'max'=>45),
			array('voucher_no', 'length', 'max'=>12),
			array('tgl', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_member, nama_transaksi, voucher_no, tgl', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idMember' => array(self::BELONGS_TO, 'Pelanggan', 'id_member'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_member' => 'Id Member',
			'nama_transaksi' => 'Nama Transaksi',
			'voucher_no' => 'Voucher No',
			'tgl' => 'Tgl',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_member',$this->id_member,true);
		$criteria->compare('nama_transaksi',$this->nama_transaksi,true);
		$criteria->compare('voucher_no',$this->voucher_no,true);
		$criteria->compare('tgl',$this->tgl,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}