<?php

/**
 * This is the model class for table "pelanggan".
 *
 * The followings are the available columns in table 'pelanggan':
 * @property string $idpelanggan
 * @property string $namapelanggan
 * @property string $tgllahir
 * @property string $alamatindonesia
 * @property string $alamathk
 * @property string $nomeras2in1
 * @property string $nomermandirisahabat
 *
 * The followings are the available model relations:
 * @property Ambilvappstore15[] $ambilvappstore15s
 * @property Ambilvopenaccount[] $ambilvopenaccounts
 * @property Ambilvpulsa20[] $ambilvpulsa20s
 * @property Ambilvremittance15[] $ambilvremittance15s
 * @property Redeemvappstore15[] $redeemvappstore15s
 * @property Redeemvopenaccount[] $redeemvopenaccounts
 * @property Redeemvpulsa20[] $redeemvpulsa20s
 * @property Redeemvremittance15[] $redeemvremittance15s
 */
class Pelanggan extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Pelanggan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pelanggan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idpelanggan, namapelanggan, tgllahir, alamatindonesia, alamathk, nomeras2in1', 'required'),
			array('idpelanggan', 'length', 'max'=>9),
			array('namapelanggan', 'length', 'max'=>120),
			array('alamatindonesia, alamathk', 'length', 'max'=>1000),
			array('nomeras2in1, nomermandirisahabat', 'length', 'max'=>45),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idpelanggan, namapelanggan, tgllahir, alamatindonesia, alamathk, nomeras2in1, nomermandirisahabat', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'ambilvappstore15s' => array(self::HAS_MANY, 'Ambilvappstore15', 'id_member'),
			'ambilvopenaccounts' => array(self::HAS_MANY, 'Ambilvopenaccount', 'id_member'),
			'ambilvpulsa20s' => array(self::HAS_MANY, 'Ambilvpulsa20', 'id_member'),
			'ambilvremittance15s' => array(self::HAS_MANY, 'Ambilvremittance15', 'id_member'),
			'redeemvappstore15s' => array(self::HAS_MANY, 'Redeemvappstore15', 'id_member'),
			'redeemvopenaccounts' => array(self::HAS_MANY, 'Redeemvopenaccount', 'id_member'),
			'redeemvpulsa20s' => array(self::HAS_MANY, 'Redeemvpulsa20', 'id_member'),
			'redeemvremittance15s' => array(self::HAS_MANY, 'Redeemvremittance15', 'id_member'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idpelanggan' => 'Idpelanggan',
			'namapelanggan' => 'Namapelanggan',
			'tgllahir' => 'Tgllahir',
			'alamatindonesia' => 'Alamatindonesia',
			'alamathk' => 'Alamathk',
			'nomeras2in1' => 'Nomeras2in1',
			'nomermandirisahabat' => 'Nomermandirisahabat',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idpelanggan',$this->idpelanggan,true);
		$criteria->compare('namapelanggan',$this->namapelanggan,true);
		$criteria->compare('tgllahir',$this->tgllahir,true);
		$criteria->compare('alamatindonesia',$this->alamatindonesia,true);
		$criteria->compare('alamathk',$this->alamathk,true);
		$criteria->compare('nomeras2in1',$this->nomeras2in1,true);
		$criteria->compare('nomermandirisahabat',$this->nomermandirisahabat,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}