<?php
/* @var $this Ambilvpulsa20Controller */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Ambilvpulsa20s',
);

$this->menu=array(
	array('label'=>'Create Ambilvpulsa20', 'url'=>array('create')),
	array('label'=>'Manage Ambilvpulsa20', 'url'=>array('admin')),
);
?>

<h1>Ambilvpulsa20s</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
