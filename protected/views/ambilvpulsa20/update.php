<?php
/* @var $this Ambilvpulsa20Controller */
/* @var $model Ambilvpulsa20 */

$this->breadcrumbs=array(
	'Ambilvpulsa20s'=>array('index'),
	$model->id=>array('view','id'=>$model->id
	'Update',
);

$this->menu=array(
	array('label'=>'List Ambilvpulsa20', 'url'=>array('index')),
	array('label'=>'Create Ambilvpulsa20', 'url'=>array('create')),
	array('label'=>'View Ambilvpulsa20', 'url'=>array('view', 'id'=>
	$model->id)),
	array('label'=>'Manage Ambilvpulsa20', 'url'=>array('admin')),
);
?>

<h1>Update Ambilvpulsa20 <?php echo  $model->id ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>