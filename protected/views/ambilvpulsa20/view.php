<?php
/* @var $this Ambilvpulsa20Controller */
/* @var $model Ambilvpulsa20 */

$this->breadcrumbs=array(
	'Ambilvpulsa20s'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Ambilvpulsa20', 'url'=>array('index')),
	array('label'=>'Create Ambilvpulsa20', 'url'=>array('create')),
	array('label'=>'Update Ambilvpulsa20', 'url'=>array('update', 
	   'id'=>$model->id)),
	array('label'=>'Delete Ambilvpulsa20', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Ambilvpulsa20', 'url'=>array('admin')),
);
?>

<h1>View Ambilvpulsa20 #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'id_member',
		'nama_transaksi',
		'voucher_no',
		'tgl',
	),
)); ?>
