<?php
/* @var $this Ambilvpulsa20Controller */
/* @var $model Ambilvpulsa20 */

$this->breadcrumbs=array(
	'Ambilvpulsa20s'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Ambilvpulsa20', 'url'=>array('index')),
	array('label'=>'Manage Ambilvpulsa20', 'url'=>array('admin')),
);
?>

<h1>Create Ambilvpulsa20</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>