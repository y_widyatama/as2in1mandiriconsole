<?php
/* @var $this Redeemvappstore15Controller */
/* @var $model Redeemvappstore15 */

$this->breadcrumbs=array(
	'Redeemvappstore15s'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Redeemvappstore15', 'url'=>array('index')),
	array('label'=>'Create Redeemvappstore15', 'url'=>array('create')),
	array('label'=>'Update Redeemvappstore15', 'url'=>array('update', 
	   'id'=>$model->id)),
	array('label'=>'Delete Redeemvappstore15', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Redeemvappstore15', 'url'=>array('admin')),
);
?>

<h1>View Redeemvappstore15 #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'id_member',
		'nama_transaksi',
		'voucher_no',
		'tgl',
	),
)); ?>
