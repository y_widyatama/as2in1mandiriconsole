<?php
/* @var $this Redeemvappstore15Controller */
/* @var $model Redeemvappstore15 */

$this->breadcrumbs=array(
	'Redeemvappstore15s'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Redeemvappstore15', 'url'=>array('index')),
	array('label'=>'Manage Redeemvappstore15', 'url'=>array('admin')),
);
?>

<h1>Create Redeemvappstore15</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>