<?php
/* @var $this Redeemvappstore15Controller */
/* @var $model Redeemvappstore15 */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'redeemvappstore15-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_member'); ?>
		<?php echo $form->textField($model,'id_member',array('size'=>9,'maxlength'=>9)); ?>
		<?php echo $form->error($model,'id_member'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nama_transaksi'); ?>
		<?php echo $form->textField($model,'nama_transaksi',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'nama_transaksi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'voucher_no'); ?>
		<?php echo $form->textField($model,'voucher_no',array('size'=>12,'maxlength'=>12)); ?>
		<?php echo $form->error($model,'voucher_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tgl'); ?>
		<?php echo $form->textField($model,'tgl'); ?>
		<?php echo $form->error($model,'tgl'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->