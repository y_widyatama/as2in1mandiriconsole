<?php
/* @var $this Redeemvappstore15Controller */
/* @var $model Redeemvappstore15 */

$this->breadcrumbs=array(
	'Redeemvappstore15s'=>array('index'),
	$model->id=>array('view','id'=>$model->id
	'Update',
);

$this->menu=array(
	array('label'=>'List Redeemvappstore15', 'url'=>array('index')),
	array('label'=>'Create Redeemvappstore15', 'url'=>array('create')),
	array('label'=>'View Redeemvappstore15', 'url'=>array('view', 'id'=>
	$model->id)),
	array('label'=>'Manage Redeemvappstore15', 'url'=>array('admin')),
);
?>

<h1>Update Redeemvappstore15 <?php echo  $model->id ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>