<?php
/* @var $this Redeemvappstore15Controller */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Redeemvappstore15s',
);

$this->menu=array(
	array('label'=>'Create Redeemvappstore15', 'url'=>array('create')),
	array('label'=>'Manage Redeemvappstore15', 'url'=>array('admin')),
);
?>

<h1>Redeemvappstore15s</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
