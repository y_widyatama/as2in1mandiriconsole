<?php
/* @var $this RedeemvopenaccountController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Redeemvopenaccounts',
);

$this->menu=array(
	array('label'=>'Create Redeemvopenaccount', 'url'=>array('create')),
	array('label'=>'Manage Redeemvopenaccount', 'url'=>array('admin')),
);
?>

<h1>Redeemvopenaccounts</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
