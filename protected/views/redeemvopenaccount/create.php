<?php
/* @var $this RedeemvopenaccountController */
/* @var $model Redeemvopenaccount */

$this->breadcrumbs=array(
	'Redeemvopenaccounts'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Redeemvopenaccount', 'url'=>array('index')),
	array('label'=>'Manage Redeemvopenaccount', 'url'=>array('admin')),
);
?>

<h1>Create Redeemvopenaccount</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>