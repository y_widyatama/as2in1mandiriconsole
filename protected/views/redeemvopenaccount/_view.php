<?php
/* @var $this RedeemvopenaccountController */
/* @var $data Redeemvopenaccount */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_member')); ?>:</b>
	<?php echo CHtml::encode($data->id_member); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_transaksi')); ?>:</b>
	<?php echo CHtml::encode($data->nama_transaksi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('voucher_no')); ?>:</b>
	<?php echo CHtml::encode($data->voucher_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl')); ?>:</b>
	<?php echo CHtml::encode($data->tgl); ?>
	<br />


</div>