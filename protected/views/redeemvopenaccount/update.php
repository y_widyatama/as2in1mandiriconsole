<?php
/* @var $this RedeemvopenaccountController */
/* @var $model Redeemvopenaccount */

$this->breadcrumbs=array(
	'Redeemvopenaccounts'=>array('index'),
	$model->id=>array('view','id'=>$model->id
	'Update',
);

$this->menu=array(
	array('label'=>'List Redeemvopenaccount', 'url'=>array('index')),
	array('label'=>'Create Redeemvopenaccount', 'url'=>array('create')),
	array('label'=>'View Redeemvopenaccount', 'url'=>array('view', 'id'=>
	$model->id)),
	array('label'=>'Manage Redeemvopenaccount', 'url'=>array('admin')),
);
?>

<h1>Update Redeemvopenaccount <?php echo  $model->id ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>