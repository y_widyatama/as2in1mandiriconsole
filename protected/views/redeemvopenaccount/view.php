<?php
/* @var $this RedeemvopenaccountController */
/* @var $model Redeemvopenaccount */

$this->breadcrumbs=array(
	'Redeemvopenaccounts'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Redeemvopenaccount', 'url'=>array('index')),
	array('label'=>'Create Redeemvopenaccount', 'url'=>array('create')),
	array('label'=>'Update Redeemvopenaccount', 'url'=>array('update', 
	   'id'=>$model->id)),
	array('label'=>'Delete Redeemvopenaccount', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Redeemvopenaccount', 'url'=>array('admin')),
);
?>

<h1>View Redeemvopenaccount #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'id_member',
		'nama_transaksi',
		'voucher_no',
		'tgl',
	),
)); ?>
