<?php
/* @var $this AmbilvopenaccountController */
/* @var $model Ambilvopenaccount */

$this->breadcrumbs=array(
	'Ambilvopenaccounts'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Ambilvopenaccount', 'url'=>array('index')),
	array('label'=>'Manage Ambilvopenaccount', 'url'=>array('admin')),
);
?>

<h1>Create Ambilvopenaccount</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>