<?php
/* @var $this AmbilvopenaccountController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Ambilvopenaccounts',
);

$this->menu=array(
	array('label'=>'Create Ambilvopenaccount', 'url'=>array('create')),
	array('label'=>'Manage Ambilvopenaccount', 'url'=>array('admin')),
);
?>

<h1>Ambilvopenaccounts</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
