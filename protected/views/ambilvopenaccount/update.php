<?php
/* @var $this AmbilvopenaccountController */
/* @var $model Ambilvopenaccount */

$this->breadcrumbs=array(
	'Ambilvopenaccounts'=>array('index'),
	$model->id=>array('view','id'=>$model->id
	'Update',
);

$this->menu=array(
	array('label'=>'List Ambilvopenaccount', 'url'=>array('index')),
	array('label'=>'Create Ambilvopenaccount', 'url'=>array('create')),
	array('label'=>'View Ambilvopenaccount', 'url'=>array('view', 'id'=>
	$model->id)),
	array('label'=>'Manage Ambilvopenaccount', 'url'=>array('admin')),
);
?>

<h1>Update Ambilvopenaccount <?php echo  $model->id ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>