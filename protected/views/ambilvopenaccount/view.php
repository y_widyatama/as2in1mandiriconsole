<?php
/* @var $this AmbilvopenaccountController */
/* @var $model Ambilvopenaccount */

$this->breadcrumbs=array(
	'Ambilvopenaccounts'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Ambilvopenaccount', 'url'=>array('index')),
	array('label'=>'Create Ambilvopenaccount', 'url'=>array('create')),
	array('label'=>'Update Ambilvopenaccount', 'url'=>array('update', 
	   'id'=>$model->id)),
	array('label'=>'Delete Ambilvopenaccount', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Ambilvopenaccount', 'url'=>array('admin')),
);
?>

<h1>View Ambilvopenaccount #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'id_member',
		'nama_transaksi',
		'voucher_no',
		'tgl_ambil',
	),
)); ?>
