<?php
/* @var $this Ambilvappstore15Controller */
/* @var $model Ambilvappstore15 */

$this->breadcrumbs=array(
	'Ambilvappstore15s'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Ambilvappstore15', 'url'=>array('index')),
	array('label'=>'Manage Ambilvappstore15', 'url'=>array('admin')),
);
?>

<h1>Create Ambilvappstore15</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>