<?php
/* @var $this Ambilvappstore15Controller */
/* @var $model Ambilvappstore15 */

$this->breadcrumbs=array(
	'Ambilvappstore15s'=>array('index'),
	$model->id=>array('view','id'=>$model->id
	'Update',
);

$this->menu=array(
	array('label'=>'List Ambilvappstore15', 'url'=>array('index')),
	array('label'=>'Create Ambilvappstore15', 'url'=>array('create')),
	array('label'=>'View Ambilvappstore15', 'url'=>array('view', 'id'=>
	$model->id)),
	array('label'=>'Manage Ambilvappstore15', 'url'=>array('admin')),
);
?>

<h1>Update Ambilvappstore15 <?php echo  $model->id ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>