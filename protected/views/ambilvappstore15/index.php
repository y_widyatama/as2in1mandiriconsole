<?php
/* @var $this Ambilvappstore15Controller */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Ambilvappstore15s',
);

$this->menu=array(
	array('label'=>'Create Ambilvappstore15', 'url'=>array('create')),
	array('label'=>'Manage Ambilvappstore15', 'url'=>array('admin')),
);
?>

<h1>Ambilvappstore15s</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
