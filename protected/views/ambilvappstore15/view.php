<?php
/* @var $this Ambilvappstore15Controller */
/* @var $model Ambilvappstore15 */

$this->breadcrumbs=array(
	'Ambilvappstore15s'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Ambilvappstore15', 'url'=>array('index')),
	array('label'=>'Create Ambilvappstore15', 'url'=>array('create')),
	array('label'=>'Update Ambilvappstore15', 'url'=>array('update', 
	   'id'=>$model->id)),
	array('label'=>'Delete Ambilvappstore15', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Ambilvappstore15', 'url'=>array('admin')),
);
?>

<h1>View Ambilvappstore15 #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'id_member',
		'nama_transaksi',
		'voucher_no',
		'tgl',
	),
)); ?>
