<?php
/* @var $this PelangganController */
/* @var $model Pelanggan */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'idpelanggan'); ?>
		<?php echo $form->textField($model,'idpelanggan',array('size'=>9,'maxlength'=>9)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'namapelanggan'); ?>
		<?php echo $form->textField($model,'namapelanggan',array('size'=>60,'maxlength'=>120)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tgllahir'); ?>
		<?php echo $form->textField($model,'tgllahir'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'alamatindonesia'); ?>
		<?php echo $form->textField($model,'alamatindonesia',array('size'=>60,'maxlength'=>1000)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'alamathk'); ?>
		<?php echo $form->textField($model,'alamathk',array('size'=>60,'maxlength'=>1000)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nomeras2in1'); ?>
		<?php echo $form->textField($model,'nomeras2in1',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nomermandirisahabat'); ?>
		<?php echo $form->textField($model,'nomermandirisahabat',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->