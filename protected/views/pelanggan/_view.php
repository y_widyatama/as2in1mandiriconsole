<?php
/* @var $this PelangganController */
/* @var $data Pelanggan */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idpelanggan')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idpelanggan), array('view', 'id'=>$data->idpelanggan)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('namapelanggan')); ?>:</b>
	<?php echo CHtml::encode($data->namapelanggan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgllahir')); ?>:</b>
	<?php echo CHtml::encode($data->tgllahir); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alamatindonesia')); ?>:</b>
	<?php echo CHtml::encode($data->alamatindonesia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alamathk')); ?>:</b>
	<?php echo CHtml::encode($data->alamathk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nomeras2in1')); ?>:</b>
	<?php echo CHtml::encode($data->nomeras2in1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nomermandirisahabat')); ?>:</b>
	<?php echo CHtml::encode($data->nomermandirisahabat); ?>
	<br />


</div>