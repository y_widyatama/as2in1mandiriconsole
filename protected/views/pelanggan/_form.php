<?php
/* @var $this PelangganController */
/* @var $model Pelanggan */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'pelanggan-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'idpelanggan'); ?>
		<?php echo $form->textField($model,'idpelanggan',array('size'=>9,'maxlength'=>9)); ?>
		<?php echo $form->error($model,'idpelanggan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'namapelanggan'); ?>
		<?php echo $form->textField($model,'namapelanggan',array('size'=>60,'maxlength'=>120)); ?>
		<?php echo $form->error($model,'namapelanggan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tgllahir'); ?>
		<?php echo $form->textField($model,'tgllahir'); ?>
		<?php echo $form->error($model,'tgllahir'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alamatindonesia'); ?>
		<?php echo $form->textField($model,'alamatindonesia',array('size'=>60,'maxlength'=>1000)); ?>
		<?php echo $form->error($model,'alamatindonesia'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alamathk'); ?>
		<?php echo $form->textField($model,'alamathk',array('size'=>60,'maxlength'=>1000)); ?>
		<?php echo $form->error($model,'alamathk'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nomeras2in1'); ?>
		<?php echo $form->textField($model,'nomeras2in1',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'nomeras2in1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nomermandirisahabat'); ?>
		<?php echo $form->textField($model,'nomermandirisahabat',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'nomermandirisahabat'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->