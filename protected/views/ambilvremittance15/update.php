<?php
/* @var $this Ambilvremittance15Controller */
/* @var $model Ambilvremittance15 */

$this->breadcrumbs=array(
	'Ambilvremittance15s'=>array('index'),
	$model->id=>array('view','id'=>$model->id
	'Update',
);

$this->menu=array(
	array('label'=>'List Ambilvremittance15', 'url'=>array('index')),
	array('label'=>'Create Ambilvremittance15', 'url'=>array('create')),
	array('label'=>'View Ambilvremittance15', 'url'=>array('view', 'id'=>
	$model->id)),
	array('label'=>'Manage Ambilvremittance15', 'url'=>array('admin')),
);
?>

<h1>Update Ambilvremittance15 <?php echo  $model->id ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>