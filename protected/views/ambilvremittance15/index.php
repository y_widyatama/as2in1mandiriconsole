<?php
/* @var $this Ambilvremittance15Controller */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Ambilvremittance15s',
);

$this->menu=array(
	array('label'=>'Create Ambilvremittance15', 'url'=>array('create')),
	array('label'=>'Manage Ambilvremittance15', 'url'=>array('admin')),
);
?>

<h1>Ambilvremittance15s</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
