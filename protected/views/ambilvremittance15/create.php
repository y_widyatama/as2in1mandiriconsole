<?php
/* @var $this Ambilvremittance15Controller */
/* @var $model Ambilvremittance15 */

$this->breadcrumbs=array(
	'Ambilvremittance15s'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Ambilvremittance15', 'url'=>array('index')),
	array('label'=>'Manage Ambilvremittance15', 'url'=>array('admin')),
);
?>

<h1>Create Ambilvremittance15</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>