<?php
/* @var $this Ambilvremittance15Controller */
/* @var $model Ambilvremittance15 */

$this->breadcrumbs=array(
	'Ambilvremittance15s'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Ambilvremittance15', 'url'=>array('index')),
	array('label'=>'Create Ambilvremittance15', 'url'=>array('create')),
	array('label'=>'Update Ambilvremittance15', 'url'=>array('update', 
	   'id'=>$model->id)),
	array('label'=>'Delete Ambilvremittance15', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Ambilvremittance15', 'url'=>array('admin')),
);
?>

<h1>View Ambilvremittance15 #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'id_member',
		'nama_transaksi',
		'voucher_no',
		'tgl',
	),
)); ?>
