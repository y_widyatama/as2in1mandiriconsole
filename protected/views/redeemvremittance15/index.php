<?php
/* @var $this Redeemvremittance15Controller */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Redeemvremittance15s',
);

$this->menu=array(
	array('label'=>'Create Redeemvremittance15', 'url'=>array('create')),
	array('label'=>'Manage Redeemvremittance15', 'url'=>array('admin')),
);
?>

<h1>Redeemvremittance15s</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
