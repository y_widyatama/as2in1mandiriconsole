<?php
/* @var $this Redeemvremittance15Controller */
/* @var $model Redeemvremittance15 */

$this->breadcrumbs=array(
	'Redeemvremittance15s'=>array('index'),
	$model->id=>array('view','id'=>$model->id
	'Update',
);

$this->menu=array(
	array('label'=>'List Redeemvremittance15', 'url'=>array('index')),
	array('label'=>'Create Redeemvremittance15', 'url'=>array('create')),
	array('label'=>'View Redeemvremittance15', 'url'=>array('view', 'id'=>
	$model->id)),
	array('label'=>'Manage Redeemvremittance15', 'url'=>array('admin')),
);
?>

<h1>Update Redeemvremittance15 <?php echo  $model->id ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>