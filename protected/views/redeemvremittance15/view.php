<?php
/* @var $this Redeemvremittance15Controller */
/* @var $model Redeemvremittance15 */

$this->breadcrumbs=array(
	'Redeemvremittance15s'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Redeemvremittance15', 'url'=>array('index')),
	array('label'=>'Create Redeemvremittance15', 'url'=>array('create')),
	array('label'=>'Update Redeemvremittance15', 'url'=>array('update', 
	   'id'=>$model->id)),
	array('label'=>'Delete Redeemvremittance15', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Redeemvremittance15', 'url'=>array('admin')),
);
?>

<h1>View Redeemvremittance15 #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'id_member',
		'nama_transaksi',
		'voucher_no',
		'tgl',
	),
)); ?>
