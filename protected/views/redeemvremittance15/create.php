<?php
/* @var $this Redeemvremittance15Controller */
/* @var $model Redeemvremittance15 */

$this->breadcrumbs=array(
	'Redeemvremittance15s'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Redeemvremittance15', 'url'=>array('index')),
	array('label'=>'Manage Redeemvremittance15', 'url'=>array('admin')),
);
?>

<h1>Create Redeemvremittance15</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>